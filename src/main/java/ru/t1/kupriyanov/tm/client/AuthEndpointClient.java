package ru.t1.kupriyanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kupriyanov.tm.dto.request.*;
import ru.t1.kupriyanov.tm.dto.response.*;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {;
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }



//    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
//        super(client);
//    }
//
//    @NotNull
//    @Override
//    public UserLoginResponse login(@NotNull UserLoginRequest request) {
//        return call(request, UserLoginResponse.class);
//    }
//
//    @NotNull
//    @Override
//    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
//        return call(request, UserLogoutResponse.class);
//    }
//
//    @NotNull
//    @Override
//    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
//        return call(request, UserProfileResponse.class);
//    }
//
//    @SneakyThrows
//    public static void main(String[] args) {
//        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
//        authEndpointClient.connect();
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//
//        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")).getSuccess());
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//
//        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
//        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
//
//        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
//        authEndpointClient.disconnect();
//    }

}
