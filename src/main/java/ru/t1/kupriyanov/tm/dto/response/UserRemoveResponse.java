package ru.t1.kupriyanov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable User user) {
        super(user);
    }

}
