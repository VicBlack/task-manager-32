package ru.t1.kupriyanov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.User;

public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable User user) {
        super(user);
    }

}
