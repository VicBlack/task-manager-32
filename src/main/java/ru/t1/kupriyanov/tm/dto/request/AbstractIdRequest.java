package ru.t1.kupriyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public AbstractIdRequest(@Nullable String id) {
        this.id = id;
    }
}
