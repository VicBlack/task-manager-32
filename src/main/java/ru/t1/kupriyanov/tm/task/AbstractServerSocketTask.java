package ru.t1.kupriyanov.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    @Nullable
    protected String userId = null;

    public AbstractServerSocketTask (
            @NotNull Server server,
            @NotNull Socket socket
            ) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask (
            @NotNull Server server,
            @NotNull Socket socket,
            @Nullable final String userId
    ) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
