package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.ProjectSort;
import ru.t1.kupriyanov.tm.enumerated.TaskSort;
import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort taskSort = TaskSort.toSort(sortType);
        @Nullable final String userId = getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, taskSort.getComparator());
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list.";
    }

}
