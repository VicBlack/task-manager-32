package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE YAML DATA FASTER]");
        getDomainService().saveDataYamlFasterXml();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to a .yaml file. faster";
    }

    @Override
    @NotNull
    public  String getName() {
        return "save-yaml-data-faster";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
