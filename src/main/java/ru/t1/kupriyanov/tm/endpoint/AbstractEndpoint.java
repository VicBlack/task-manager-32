package ru.t1.kupriyanov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.service.IServiceLocator;
import ru.t1.kupriyanov.tm.api.service.IUserService;
import ru.t1.kupriyanov.tm.dto.request.AbstractUserRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.exception.user.AccessDeniedException;
import ru.t1.kupriyanov.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@NotNull AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (userId == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
    }

}
