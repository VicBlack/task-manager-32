package ru.t1.kupriyanov.tm.exception.field;

public final class UserIdEmptyException extends AbstractFiledException {

    public UserIdEmptyException() {
        super("Error! User Id is incorrect!");
    }

}
