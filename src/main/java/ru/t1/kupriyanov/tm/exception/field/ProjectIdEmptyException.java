package ru.t1.kupriyanov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFiledException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
